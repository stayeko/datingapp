import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-valeur',
  templateUrl: './valeur.component.html',
  styleUrls: ['./valeur.component.css']
})
export class ValeurComponent implements OnInit {
  valeur: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {

    this.http.get('http://localhost:5000/api/valeur').subscribe(response => {this.valeur = response; }, error => {console.log(error); });
 }

}
